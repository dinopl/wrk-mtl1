import Control.Monad (when)
import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State
import System.Environment (getArgs)


-- environment for Reader
data Config = Config String
  deriving Eq

-- state for State
data Auth
  = Authenticated
  | Unauthenticated
  deriving (Eq, Show)


type App a = ReaderT Config (ExceptT String (StateT Auth IO)) a

runApp :: Config -> Auth -> App a -> IO (Either String a, Auth)
runApp env st ev = runStateT (runExceptT (runReaderT ev env)) st


-- authenticate :: App ()
--   or with class constraints
authenticate :: (MonadIO m, MonadReader Config m, MonadState Auth m) => m ()
authenticate = do
  config' <- ask
  when (config' == (Config "authok")) $ put Authenticated
  pure ()


main :: IO ()
main = do
  config <- (Config . head) <$> getArgs

  -- This do block represents the top-level of the work being done under the App monad
  result <- runApp config Unauthenticated $ do
    authenticate
    state' <- get
    when (state' == Unauthenticated) $ throwError "Failure has occurred!"

  -- result is what happened at the end of the run
  print result
